# Blockquote

## Design Notes
Out of the box, the blockquote component supports quick customization of the following properties:

- `background`, `color`, `margin`,`padding`, and `max-width` of the blockquote element
- `font-family`, `font-size`, `font-weight`, `line-height`, and `content` of the opening quotation icon
- `font-family`, `font-size`, `font-weight`, `line-height`, `margin`, and `padding` of the quote content
- `font-family`, `margin`, and `padding` of the blockquote's footer
- `font-size` and `font-weight` of the author's name
- `font-size` and `font-weight` of the source
- `width` and aspect ratio of the author's portrait