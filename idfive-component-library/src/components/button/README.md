# Button

## Design Notes
Out of the box, the button component supports quick customization of the following properties:

- `background`, `color`, `border`, `font-family`, `font-weight`, `text-decoration`, `text-transform`, `text-align`, `padding`, and `transitions`
- `background`, `color`, and `border` when hovering over the button