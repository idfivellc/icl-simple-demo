# Carousel

## Design Notes
Out of the box, the carousel component supports quick customization of the following properties:

- Aspect ratio of the slide images
- `height`, `width`, `background-color`, and `transition` of the previous and next buttons
- `background-color` of the previous and next buttons when hovered over
- `color` and `transition` of the arrows in the previous and next buttons
- `color` of the arrows in the previous and next buttons when hovering over the button