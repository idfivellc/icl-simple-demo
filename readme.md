# Quickstart ICL steps

## Requires

- NPM
- NVM

## General setup

- Download [idfive Component Library](https://bitbucket.org/idfivellc/idfive-component-library/src/master/) into desired repo. This functions as the starter, but is designed to be modified directly/added to/etc.
- $ `cd idfive-component-library`.
- $ `rm -R .git`. Remove the repo git, as the desire is to commit all to your new repo.
- $ `nvm use`. (sets node to stable a version, 12.20.0)
- $ `npm install`. (installs all npm dependencies)

## Component development: Spin up fractal

For component based development, fractal is installed and set up.

- $ `nvm use` (if haven't already, from within idfive-component-library)
- $ `npm run fractal` Runs fractal tasks, and brings up a frontend view at: http://localhost:3000

## Integrations into cms templates/etc: Watch files, load a local env for whatever you are integrating

- $ `nvm use` (if haven't already, from within idfive-component-library)
- $ `npm run watch`. watches scss and js files, rebuilds on save of either.
- $ `lando start` (or whatever local env you are running).
- See index.html example in root of repo.

### Important notes for integration

- Add both screen, and print stylesheets in head. (see index.html)
- Add js after body. (see index.html)
